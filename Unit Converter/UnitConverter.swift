//
//  Unit Converter.swift
//  Unit Converter
//
//  Created by student on 9/5/16.
//  Copyright © 2016 WeW. All rights reserved.
//

import Foundation

class UnitConverter {
    func degreesFahrenheit(degressCelsius: Int) -> Int {
        return Int(1.8 * Float(degressCelsius) + 32.0)
    }
}