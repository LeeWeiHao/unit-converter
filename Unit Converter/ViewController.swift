//
//  ViewController.swift
//  Unit Converter
//
//  Created by student on 9/5/16.
//  Copyright © 2016 WeW. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UIPickerViewDelegate {
    

     private var converter = UnitConverter()
   
    @IBOutlet var temperatureRange: TemperatureRange!

    @IBOutlet weak var temperatureLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let celciusValue = temperatureRange.values[row]
        return "\(celciusValue)℃"
        
        //return "NºC"
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // convert and display temperature
        temperatureLabel.text = "\(Int(converter.degreesFahrenheit(temperatureRange.values[row])))℉"
    }
    
}

